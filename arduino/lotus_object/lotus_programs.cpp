

#include "lotus_programs.h"

// This is only called once when the Arduino is first started.
// Put any program specific initialization here.
void programSetup() {
  // Setup all the gradients;
  fillGradients();

  // Setup the phase for this arduino ID.
  phase_offset = float(ARDUINO_ID)/float(NUM_ARDUINO-1);
  
}

// Basic program setup.
void programInit() {
  time_program_start = time_running;
}

// This sholud get called for every program loop.
void programLoop() {
  time_program = time_running - time_program_start;
}


// Setup the startup program.
void programStartupInit() {
  programInit();
}

// This is a program to help with startup and syncing of the LEDs.
void programStartup() {
  programLoop();
  patternStartup();
}


// Setup the heartbeat program.
void programHeartbeatInit() {
  programInit();
}

// My favorite heartbeat program.
void programHeartbeat() {
  programLoop();
  patternHeartbeat();
}


void programHeartbeatMotionInit() {
  programInit();  
}

void programHeartbeatMotion() {
  programLoop();
  patternHeartbeatMotion(); 
}


void programHotaruInit(){
  programInit();
  time_pattern_start = time_running;
  pattern_current_part = -1;
  pattern_last_part = -1;
}

void programHotaru(){
  programLoop();
  patternHotaru();
}


// Setup the rainbow program.
void programGradientRainbowInit() {
  programInit();
}

// The rainbow program.
void programGradientRainbow() {
  programLoop();
  patternGradientRainbow();
}

// Setup the rainbow poprocks program.
void programGradientRainbowPoprocksInInit() {
  programInit();
  random_num = float(random(1000000)/1000000.0);
}

// The rainbow poprocks program.
void programGradientRainbowPoprocksIn() {
  programLoop();
  patternGradientRainbowPoprocks(true);
}

// Setup the rainbow poprocks program.
void programGradientRainbowPoprocksOutInit() {
  programInit();
  random_num = float(random(1000000)/1000000.0);
}

// The rainbow poprocks program.
void programGradientRainbowPoprocksOut() {
  programLoop();
  patternGradientRainbowPoprocks(false);
}

// Setup the miami sunset program.
void programGradientMiamiSunsetInit() {
  programInit();
}

// The rainbow program.
void programGradientMiamiSunset() {
  programLoop();
  patternGradientMiamiSunset();
}



#include "lotus_global.h"

unsigned long time_total = 0;
unsigned long time_running = 0;
unsigned long time_sleep = 0;
unsigned long time_program = 0;
unsigned long time_program_start = 0;
unsigned long time_fade_start = 0;
unsigned long time_delay = 0;
unsigned long time_pattern = 0;
unsigned long time_pattern_start = 0; 

// Keep track of the number of program cycles.
unsigned long cycle = 0;

// The phase of this particular arduino.  
float phase_offset = 0.0;

// RGB color values for the leds.  All connected LEDs will share this color.
unsigned char led_color[3];

// These are used to determine if a new program has started.
unsigned char current_program_num = 0;
unsigned char last_program_num = 0;

// Used for some pattern manipulations.
signed char pattern_current_part = 0;
signed char pattern_last_part = 0;

// Variables for the main loop.
bool is_sleeping = false;
bool is_fade_out = false;
bool is_fade_in = false;
bool enable_fade = true;

// Pattern variables
float random_num = 0.0;

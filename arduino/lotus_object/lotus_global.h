

#ifndef lotus_global_h
#define lotus_global_h

extern unsigned long time_total;
extern unsigned long time_running;
extern unsigned long time_sleep;
extern unsigned long time_program;
extern unsigned long time_program_start;
extern unsigned long time_fade_start;
extern unsigned long time_pattern;
extern unsigned long time_pattern_start;  

// This holds the time delay for earh LED so as to get them all synced up.
extern unsigned long time_delay;

// Keep track of the number of program cycles.
extern unsigned long cycle;

// The phase of this particular arduino.  
// This is calculated at runtime based on the arduino ID and used for spatial patterns.
// This value will go between 0.0 and 1.0.
extern float phase_offset;

// RGB color values for the leds.  All connected LEDs will share this color.
extern unsigned char led_color[3];

// These are used to determine if a new program has started.
extern unsigned char current_program_num;
extern unsigned char last_program_num;

// Used for some pattern manipulations.
extern signed char pattern_current_part;
extern signed char pattern_last_part;


// Variables for the main loop.
extern bool is_sleeping;
extern bool is_fade_out;
extern bool is_fade_in;
extern bool enable_fade;

// Pattern variables
extern float random_num;

#endif



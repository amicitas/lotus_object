

#include "lotus_patterns.h"


// Add a phase_offset to a phase where both values span between 0.0 and 1.0.
float addPhase(float phase, float phase_offset) {
  float output;
  if ((phase + phase_offset) > 1.0) {
    output = phase_offset - (1.0 - phase);
  }
  else {
    output = phase + phase_offset;
  }
  return output;
}


/* -----------
 *  This pottern is designed to help me get all the endividual arduions all in sync.
 */
void patternStartup(){
  int period_mod;
  
  period_mod = (time_program) % TIME_OFFSET;

  /*
  Serial.print("time_program: ");
  Serial.print(time_program);
  Serial.print("period_fraction: ");
  Serial.print(period_fraction);
  Serial.print("\n");
  */



  if (period_mod >= (4100)) {
    led_color[0] = 0;
    led_color[1] = 0;
    led_color[2] = 0;
  }
  else if (period_mod >= (4000)) {
    led_color[0] = 0;
    led_color[1] = 255;
    led_color[2] = 0;
  }
  else if (period_mod >= (3100)) {
    led_color[0] = 0;
    led_color[1] = 0;
    led_color[2] = 0;
  }
  else if (period_mod >= (3000)) {
    led_color[0] = 255;
    led_color[1] = 45;
    led_color[2] = 0;
  }
  else if (period_mod >= (2100)) {
    led_color[0] = 0;
    led_color[1] = 0;
    led_color[2] = 0;
  }
  else if (period_mod >= (2000)) {
    led_color[0] = 255;
    led_color[1] = 0;
    led_color[2] = 0;
  }
  else if (period_mod >= (1100)) {
    led_color[0] = 0;
    led_color[1] = 0;
    led_color[2] = 0;
  }
  else if (period_mod >= (1000)) {
    led_color[0] = 255;
    led_color[1] = 0;
    led_color[2] = 0;
  }
  else if (period_mod >= (100)) {
    led_color[0] = 0;
    led_color[1] = 0;
    led_color[2] = 0;
  }
  else if (period_mod >= (0)) {
    led_color[0] = 0;
    led_color[1] = 0;
    led_color[2] = 0;
  }
  else {
    led_color[0] = 0;
    led_color[1] = 0;
    led_color[2] = 255;    
  }
};


void patternHeartbeat(){
  const int period = 5000;
  int period_fraction = 0;
  float period_dummy = 0.0;
  float value = 0.0;
  
  period_fraction = (((time_program) % period)*1000) / period;

  if (period_fraction > 900) {
    led_color[0] = 0;
    led_color[1] = 0;
    led_color[2] = 0;
  }
  else if (period_fraction > 600) {
    period_dummy = float(period_fraction - 600)/300.0*PI;
    value = (cos(period_dummy)+1.0)/2.0;
    led_color[0] = int(value*255);
    led_color[1] = 0;
    led_color[2] = 0;
  }
  else if (period_fraction > 400) {
    period_dummy = float(period_fraction - 400)/200.0*PI;
    period_dummy += PI;
    value = (cos(period_dummy)+1.0)/2.0;
    led_color[0] = int(value*255);
    led_color[1] = 0;
    led_color[2] = 0;  
  }
  else if (period_fraction > 200) {
    period_dummy = float(period_fraction - 200)/200.0*PI; 
    value = (cos(period_dummy)+1.0)/2.0;
    led_color[0] = int(value*255);
    led_color[1] = 0;
    led_color[2] = 0;
  }
  else {
    period_dummy = float(period_fraction - 0)/200.0*PI;
    period_dummy += PI;
    value = (cos(period_dummy)+1.0)/2.0;
    led_color[0] = int(value*255);
    led_color[1] = 0;
    led_color[2] = 0;    
  }

  /*
  Serial.print("period_fraction: ");
  Serial.print(period_fraction);
  Serial.print("period_dummy: ");
  Serial.print(period_dummy);
  Serial.print("\n");
  */
};



void patternHeartbeatMotion(){
  const int period = 5000;
  const int period_motion = 20000;

  float phase_position = 0.0;
  float phase = 0.0;
  float phase_motion = 0.0;
  float period_dummy = 0.0;
  float value = 0.0;
  
  phase = (float((time_program) % period)) / period;

  // I want the motion phase to occilate between 0 and 1
  phase_motion = sq(sin((float((time_running - time_program) % period_motion)) / period_motion * PI));

  // The pattern should be symmetric on both sides of the balloon with ID=0.
  if (ARDUINO_ID <= (NUM_ARDUINO/2 -1)) {
    phase_position = float(ARDUINO_ID-1)/float(NUM_ARDUINO)/2.0;
  } 
  else {
    phase_position = float(NUM_ARDUINO - ARDUINO_ID - 1)/float(NUM_ARDUINO)/2.0;
  }

  // Set the phase to span between the original phase, and the phase_motion
  // as we go around the circle of LEDs.
  phase = addPhase(phase, phase_motion*phase_position);
  
  if (phase > 0.9) {
    led_color[0] = 0;
    led_color[1] = 0;
    led_color[2] = 0;
  }
  else if (phase > 0.6) {
    period_dummy = (phase - 0.6)/0.3*PI;
    value = (cos(period_dummy)+1.0)/2.0;
    led_color[0] = int(value*255);
    led_color[1] = 0;
    led_color[2] = 0;
  }
  else if (phase > 0.4) {
    period_dummy = (phase - 0.4)/0.2*PI;
    period_dummy += PI;
    value = (cos(period_dummy)+1.0)/2.0;
    led_color[0] = int(value*255);
    led_color[1] = 0;
    led_color[2] = 0;  
  }
  else if (phase > 0.2) {
    period_dummy = (phase - 0.2)/0.2*PI; 
    value = (cos(period_dummy)+1.0)/2.0;
    led_color[0] = int(value*255);
    led_color[1] = 0;
    led_color[2] = 0;
  }
  else {
    period_dummy = (phase - 0)/0.2*PI;
    period_dummy += PI;
    value = (cos(period_dummy)+1.0)/2.0;
    led_color[0] = int(value*255);
    led_color[1] = 0;
    led_color[2] = 0;    
  }
     
  /*
  Serial.print("phase: ");
  Serial.print(phase);
  Serial.print("period_dummy: ");
  Serial.print(period_dummy);
  Serial.print("\n");
  */
};


void patternPoprocks(bool direction_in){
  const int period = 10000;
  const int random_amp = 1000;
  int random_offset = 0;
  float phase_position = 0.0;
  bool enable_led = false;

  // The turn on pattern should be symmetric on both sides of the balloon with ID=0.
  if (ARDUINO_ID <= (NUM_ARDUINO/2 -1)) {
    phase_position = float(ARDUINO_ID-1)/float(NUM_ARDUINO)/2.0;
  } 
  else {
    phase_position = float(NUM_ARDUINO - ARDUINO_ID - 1)/float(NUM_ARDUINO)/2.0;
  }

  // If we are doing a fade out, then start from the far side.
  if (! direction_in) {
    phase_position = 1 - phase_position;
  }
  
  // Square the phase so that I get an accelerating effect.
  phase_position = sq(phase_position);

  // Randomness should be bigger for the first baloons, and zero by the end.
  random_offset = (random_amp * (1 - phase_position))*random_num;


  enable_led = (time_program) > (phase_position*period + random_offset);

  if (! direction_in) {
    enable_led = (! enable_led);
  }
  
  // Leave the LED off until the appropriate time. 
  if (! enable_led) {
    led_color[0] = 0;
    led_color[1] = 0;
    led_color[2] = 0;
  }
};


void patternGradientRainbow(){
  const int period = 10000;
  float phase = 0.0;

  phase = (float((time_program) % period) / float(period));
  
  setColorGradientRainbow(addPhase(phase, phase_offset), led_color);

};


// Pop in or out a Gradent Rainbow color.
void patternGradientRainbowPoprocks(bool direction_in){
  // Set the color. Initially the color is static.
  setColorGradientRainbow(phase_offset, led_color);

  // Now run the paprocks pattern.
  patternPoprocks(direction_in);
};


// A rotating Gradient Miami Sunset color
void patternGradientMiamiSunset(){
  const int period = 10000;
  float phase = 0;

  phase = (float((time_program) % period) / float(period));

  setColorGradientMiamiSunset(addPhase(phase, phase_offset), led_color);
};


// Pop in or out a Gradent Miami Sunset color.
void patternGradientMiamiSunsetPoprocks(bool direction_in){
  // Set the color. Initially the color is static.
  setColorGradientMiamiSunset(phase_offset, led_color);

  // Now run the paprocks pattern.
  patternPoprocks(direction_in);
};


// A Hotaru pattern.  Lights will fade in and out randomly.
void patternHotaru(){
  int period = 6000;
  int period_fraction = 0;
  float period_dummy = 0.0;
  float value = 0.0;
  
  unsigned long time_off = 0;
  unsigned char color[3];

  time_off = random_num * 10000;
  time_pattern = time_running - time_pattern_start;

  // Initialize the pattern loop.
  if ((pattern_current_part < 0) or (time_pattern < time_off) or (time_pattern > (period + time_off))) {
    pattern_current_part = 0;
    if (pattern_current_part != pattern_last_part) {
      random_num = float(random(1000000)/1000000.0);
      time_pattern_start = time_running;
      time_pattern = 0;
      time_off = random_num * 10000;
    }
  }

  
  // Setup the default color.
  color[0] = 175;
  color[1] = 255;
  color[2] = 0;

  
  if (time_pattern < time_off) {
    // Off.
    led_color[0] = 0;
    led_color[1] = 0;
    led_color[2] = 0;
  }
  else if (time_pattern < (time_off + 3000)) {
    // Rising.
    pattern_current_part = 1;
    period_dummy = float((time_pattern - time_off))/3000.0*PI;

    period_dummy += PI;
    value = (cos(period_dummy)+1.0)/2.0;
    for (int ii=0; ii <3; ii++) {
      led_color[ii] = (unsigned char)(value*color[ii]);
    }  
  }
  else if (time_pattern < (time_off + 5000)) {
    // On.
    pattern_current_part = 2;
    for (int ii=0; ii <3; ii++) {
      led_color[ii] = color[ii];
    }  
  }
  else if (time_pattern < (time_off + 6000)) {
    // Falling.
    pattern_current_part = 3;
    period_dummy = float((time_pattern - time_off)-5000)/1000.0*PI;
    
    value = (cos(period_dummy)+1.0)/2.0;
    for (int ii=0; ii <3; ii++) {
      led_color[ii] = (unsigned char)(value*color[ii]);
    }
  }
  else {
    // We should never get here, but just incase I messed something up.
    pattern_current_part = -1;
    led_color[0] = 0;
    led_color[1] = 0;
    led_color[2] = 0;
  }

  /*
  Serial.print(" time_pattern: ");
  Serial.print(time_pattern);
  Serial.print(" time_off: ");
  Serial.print(time_off);
  Serial.print(" part: ");
  Serial.print(pattern_current_part);
  Serial.print(" period_dummy: ");
  Serial.print(period_dummy);
  Serial.print(" value: ");
  Serial.print(value);
  Serial.print("\n");
  */
  
  pattern_last_part = pattern_current_part;
};



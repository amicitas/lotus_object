
#include "lotus_colors.h"

unsigned char gradient_miami_sunset[6][4];
unsigned char gradient_rainbow[7][4];


void fillGradientMiamiSunset() {
  gradient_miami_sunset[0][0] = 0;
  gradient_miami_sunset[1][0] = 30;
  gradient_miami_sunset[2][0] = 63;
  gradient_miami_sunset[3][0] = 127;
  gradient_miami_sunset[4][0] = 191;
  gradient_miami_sunset[5][0] = 255;

  
  // Red
  gradient_miami_sunset[0][1] = 0;
  gradient_miami_sunset[1][1] = 255;
  gradient_miami_sunset[2][1] = 255;
  gradient_miami_sunset[3][1] = 255;
  gradient_miami_sunset[4][1] = 255;
  gradient_miami_sunset[5][1] = 0;

  // Green
  gradient_miami_sunset[0][2] = 0;
  gradient_miami_sunset[1][2] = 0;
  gradient_miami_sunset[2][2] = 255;
  gradient_miami_sunset[3][2] = 127;
  gradient_miami_sunset[4][2] = 0;
  gradient_miami_sunset[5][2] = 0;

  // Blue
  gradient_miami_sunset[0][3] = 255;
  gradient_miami_sunset[1][3] = 127;
  gradient_miami_sunset[2][3] = 0;
  gradient_miami_sunset[3][3] = 0;
  gradient_miami_sunset[4][3] = 255;
  gradient_miami_sunset[5][3] = 255;

};

void fillGradientRainbow() {
  
  gradient_rainbow[0][0] = 0;
  gradient_rainbow[1][0] = 43;
  gradient_rainbow[2][0] = 63;
  gradient_rainbow[3][0] = 107;
  gradient_rainbow[4][0] = 147;
  gradient_rainbow[5][0] = 211;
  gradient_rainbow[6][0] = 255;

  //red
  gradient_rainbow[0][1] = 255;
  gradient_rainbow[1][1] = 255;
  gradient_rainbow[2][1] = 255;
  gradient_rainbow[3][1] = 0;
  gradient_rainbow[4][1] = 0;
  gradient_rainbow[5][1] = 127;
  gradient_rainbow[6][1] = 255;

  //green
  gradient_rainbow[0][2] = 0;
  gradient_rainbow[1][2] = 127;
  gradient_rainbow[2][2] = 255;
  gradient_rainbow[3][2] = 255;
  gradient_rainbow[4][2] = 0;
  gradient_rainbow[5][2] = 0;
  gradient_rainbow[6][2] = 0;

  //blue
  gradient_rainbow[0][3] = 0;
  gradient_rainbow[1][3] = 0;
  gradient_rainbow[2][3] = 0;
  gradient_rainbow[3][3] = 0;
  gradient_rainbow[4][3] = 255;
  gradient_rainbow[5][3] = 255;
  gradient_rainbow[6][3] = 0;
  
}


// Fill all available gradients.
void fillGradients() {
  fillGradientMiamiSunset(); 
  fillGradientRainbow();
}

// Use this funciton to generate colors from the Miami Sunset Gradient.
void setColorGradientMiamiSunset(float value, unsigned char color[3]) {
  char len;
 
  // Get the number of parts in the gradient.
  len = sizeof(gradient_miami_sunset)/sizeof(gradient_miami_sunset[0]);
 
  setColorGradient(value, color, gradient_miami_sunset, len);
}


// Use this funciton to generate colors from the Rainbow Gradient.
void setColorGradientRainbow(float value, unsigned char color[3]) {
  char len;
  
  // Get the number of parts in the gradient.
  len = sizeof(gradient_rainbow)/sizeof(gradient_miami_sunset[0]);

  setColorGradient(value, color, gradient_rainbow, len);
}

void setColorGradient(float value, unsigned char color[3], unsigned char gradient[][4], unsigned char len) {

  int ii;
  int index;
  float fraction;


  // First figure out what section we are in.
  for (ii = 0;  ii<len; ii++) {
    if (value*255.0 <= gradient[ii][0]){
      index = ii-1;
      break;
    }
  }

  fraction = (value*255.0 - float(gradient[index][0]))/float(gradient[index+1][0] - gradient[index][0]);

  for (ii = 0; ii<3; ii++) {
    color[ii] = (float(gradient[index+1][ii+1] - gradient[index][ii+1])*fraction) + gradient[index][ii+1];
  }

};



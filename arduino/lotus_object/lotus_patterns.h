
#ifndef lotus_patterns_h
#define lotus_patterns_h

#include "Arduino.h"
#include "lotus_options.h"
#include "lotus_global.h"
#include "lotus_colors.h"

float addPhase(float, float);

void patternStartup();

void patternHeartbeat();
void patternHeartbeatMotion();

void patternPoprocks(bool);

void patternGradientRainbow();
void patternGradientRainbowPoprocks(bool);

void patternGradientMiamiSunset();
void patternGradientMiamiSunsetPoprocks(bool);

void patternHotaru();


#endif




#ifndef lotus_programs_h
#define lotus_programs_h

#include "lotus_options.h"
#include "lotus_global.h"
#include "lotus_patterns.h"
#include "lotus_colors.h"

void programSetup();
void programInit();

void programStartupInit();
void programStartup();

void programHeartbeatInit();
void programHeartbeat();

void programHotaruInit();
void programHotaru();

void programHeartbeatMotionInit();
void programHeartbeatMotion();

void programGradientMiamiSunsetInit();
void programGradientMiamiSunset();

void programGradientRainbowInit();
void programGradientRainbow();

void programGradientRainbowPoprocksInInit();
void programGradientRainbowPoprocksIn();

void programGradientRainbowPoprocksOutInit();
void programGradientRainbowPoprocksOut();


#endif


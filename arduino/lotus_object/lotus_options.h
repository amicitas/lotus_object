

#ifndef lotus_options_h
#define lotus_options_h

// Define an id for this particular arduino balloon.  This should match the label.
// Numbering starts from 0.
const unsigned char ARDUINO_ID = 5;

// Total number of installed ballons.
const unsigned char NUM_ARDUINO = 10;

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
const int PIN = 2;

// How many NeoPixels are attached to the Arduino?
const int NUMPIXELS = 1;

const unsigned long FADE_LENGTH = 5000;

// This is the time setting for pressing the reset button and getting all the LEDs in sync.
const unsigned long TIME_OFFSET = 5000;

const unsigned long RESET_DELAY = 0;

#endif


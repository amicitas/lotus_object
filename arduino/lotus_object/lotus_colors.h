


#ifndef lotus_color_h
#define lotus_color_h


#include "Arduino.h"

extern unsigned char gradient_miami_sunset[6][4];
extern unsigned char gradient_rainbow[7][4];

void fillGradientMiamiSunset();
void fillGradientRainbaw();
void fillGradients();


void setColorGradientMiamiSunset(float value, unsigned char color[3]);
void setColorGradientRainbow(float value, unsigned char color[3]);
void setColorGradient(float value, unsigned char color[3], unsigned char gradient[][4], unsigned char len);
  

#endif


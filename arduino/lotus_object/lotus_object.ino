// Authors:
//  Novimir Antoniuk Pablant
//    amicitas@gmail.com
//
// Description:
//   This is currently just a bit of example code to show WS2812B LED programing
//   and power management using an Arduino Nano ATmega168.
//
// Memory Usage:
//   The power management by itself uses 1000 bytes of program storage space.
//
// Setup Notes:
//   The layout should have the balloon with ID=0 at the back of the installation.
//   The ballons should then be placed in order around the circle in a counter-
//   clockwise manner. 

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>
#endif

#include "lotus_options.h"
#include "lotus_global.h"

#include "lotus_programs.h"

// ===========================
// Begin Power Management Code
// ===========================


// Watchdog Interrupt Service. This is executed when watchdog timed out.
ISR(WDT_vect) {
  wdt_disable();
}

// Enters the arduino into sleep mode.
void enterSleep(void)
{
  setupWatchDogTimer();

  // There are five different sleep modes in order of power saving:
  // SLEEP_MODE_IDLE - the lowest power saving mode
  // SLEEP_MODE_ADC
  // SLEEP_MODE_PWR_SAVE
  // SLEEP_MODE_STANDBY
  // SLEEP_MODE_PWR_DOWN - the highest power saving mode
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();

  // Now enter sleep mode.
  sleep_mode();

  // The program will continue from here after the WDT timeout

  // Disable sleep as a precaution.
  sleep_disable();

  // Re-enable the peripherals.
  //power_all_enable();

  // Update the sleep timer.
  // This line needs to be updated if we change the sleep time.
  time_sleep += 1000;
}

// Setup the Watch Dog Timer (WDT)
void setupWatchDogTimer() {
  // The MCU Status Register (MCUSR) is used to tell the cause of the last
  // reset, such as brown-out reset, watchdog reset, etc.
  // NOTE: for security reasons, there is a timed sequence for clearing the
  // WDE and changing the time-out configuration. If you don't use this
  // sequence properly, you'll get unexpected results.

  // Clear the reset flag on the MCUSR, the WDRF bit (bit 3).
  MCUSR &= ~(1 << WDRF);

  // Configure the Watchdog timer Control Register (WDTCSR)
  // The WDTCSR is used for configuring the time-out, mode of operation, etc

  // In order to change WDE or the pre-scaler, we need to set WDCE (This will
  // allow updates for 4 clock cycles).

  // Set the WDCE bit (bit 4) and the WDE bit (bit 3) of the WDTCSR. The WDCE
  // bit must be set in order to change WDE or the watchdog pre-scalers.
  // Setting the WDCE bit will allow updates to the pre-scalers and WDE for 4
  // clock cycles then it will be reset by hardware.
  WDTCSR |= (1 << WDCE) | (1 << WDE);

  /**
      Setting the watchdog pre-scaler value with VCC = 5.0V and 16mHZ
      WDP3 WDP2 WDP1 WDP0 | Number of WDT | Typical Time-out at Oscillator Cycles
      0    0    0    0    |   2K cycles   | 16 ms
      0    0    0    1    |   4K cycles   | 32 ms
      0    0    1    0    |   8K cycles   | 64 ms
      0    0    1    1    |  16K cycles   | 0.125 s
      0    1    0    0    |  32K cycles   | 0.25 s
      0    1    0    1    |  64K cycles   | 0.5 s
      0    1    1    0    |  128K cycles  | 1.0 s
      0    1    1    1    |  256K cycles  | 2.0 s
      1    0    0    0    |  512K cycles  | 4.0 s
      1    0    0    1    | 1024K cycles  | 8.0 s
  */
  WDTCSR  = (0 << WDP3) | (1 << WDP2) | (1 << WDP1) | (0 << WDP0);
  // Enable the WD interrupt (note: no reset).
  WDTCSR |= _BV(WDIE);
}

// ============================
// Begin WS2818B LED Setup Code
// ============================


// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);




// ============================
// Begin Program Code
// ============================


// This sets up the startup time delay for this individual arduino with a given ID.
void setTimeDelay() {
  time_delay = ARDUINO_ID * TIME_OFFSET;
}

// Loop through all the LEDs and set their color.
void updateLeds() {

  /*
  Serial.print("LED color: ");
  for (char ii=0; ii<3; ii++) {
    Serial.print(" ");
    Serial.print(led_color[ii]);
  }
  Serial.print("\n;");
  */
  
  // For a set of NeoPixels the first NeoPixel is 0, second is 1, all the way up to the count of pixels minus one.
  // turn the LED on (HIGH is the voltage level)
  for (byte ii = 0; ii < NUMPIXELS; ii++) {
    // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
    pixels.setPixelColor(ii, pixels.Color(led_color[0], led_color[1], led_color[2]));
  }

  pixels.show(); // This sends the updated pixel color to the hardware.
};


void startFadeOut() {
  if (!is_fade_out) {
    time_fade_start = time_total;  
  }
  is_fade_out = true;
  is_fade_in = false;
}

void endFadeOut() {
  is_fade_out = false;
}

void startFadeIn() {
  if (!is_fade_in) {
    time_fade_start = time_total;  
  }
  is_fade_in = true;
  is_fade_out = false;
}

void fadeInLeds(){
  unsigned char max_value;
  if ((time_total - time_fade_start) > FADE_LENGTH) {
    is_fade_in = false;
  }
  else {
    max_value = ((time_total - time_fade_start)*255)/FADE_LENGTH;

    for (char ii=0; ii<3; ii++) {
      if (led_color[ii] > max_value) {
        led_color[ii] = max_value;
      }
    }  
  }
}

// Fade the LEDs out.
void fadeOutLeds(){
  unsigned char max_value;
  max_value = 255 - ((time_total - time_fade_start)*255)/FADE_LENGTH;
  for (char ii=0; ii<3; ii++) {
    if (led_color[ii] > max_value) {
      led_color[ii] = max_value;
    }
  }
}


// Turn off all the LEDs immediately.
void turnOffLeds() {
  led_color[0] = 0;
  led_color[1] = 0;
  led_color[2] = 0;
  
  updateLeds();
}


void setup() {
  // This initializes the NeoPixel library.
  pixels.begin();
  // Turn off all the LEDs, just incase they are still on.
  turnOffLeds();

  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  Serial.begin(9600);

  programSetup();

  // Set the delay related to the Arduino ID.
  setTimeDelay();

  
}


// This is the main loop and also where we handle all our scheduling.
void loop() {
  auto current_program = programStartup;
  auto current_programInit = programStartupInit;
  bool is_sleeping = false;

  unsigned long program_length;
  unsigned long sum_program_length;
  
  time_running = millis();
  time_total = time_running + time_sleep;

  //Serial.print("time_running: ");
  //Serial.print(time_running);
  //Serial.print("\n");

  sum_program_length = 0;
  current_program_num = 0;

  while (true) {

    program_length = 30000;
    sum_program_length += program_length;
    current_program_num += 1;
    if (time_total < sum_program_length) {
      // Current pattern period is 5000 + Random.
      current_programInit = programHotaruInit;
      current_program = programHotaru;
      enable_fade = true;
      break;
    }

    
    // I want one extra sync cycle after the last press of the reset button.
    program_length = (NUM_ARDUINO - ARDUINO_ID + 1) * TIME_OFFSET;
    sum_program_length += program_length;
    current_program_num += 1;
    if (time_total < sum_program_length) {
      current_programInit = programStartupInit;
      current_program = programStartup;
      break;
    }

    program_length = 30000;
    sum_program_length += program_length;
    current_program_num += 1;
    if (time_total < sum_program_length) {
      // Current pattern period is 5000.
      current_programInit = programHeartbeatInit;
      current_program = programHeartbeat;
      enable_fade = true;
      break;
    }
    
    program_length = 60000;
    sum_program_length += program_length;
    current_program_num += 1;
    if (time_total < sum_program_length) {
      // Current pattern period is 20000.
      current_programInit = programHeartbeatMotionInit;
      current_program = programHeartbeatMotion;
      enable_fade = true;
      break;
    }
    
    program_length = 20000;
    sum_program_length += program_length;
    current_program_num += 1;
    if (time_total < sum_program_length) {
      current_programInit = programGradientMiamiSunsetInit;
      current_program = programGradientMiamiSunset;
      enable_fade = true;
      break;
    }

    // This program should always run for exactly one period.
    // Current period is 10000
    program_length = 10000;
    sum_program_length += program_length;
    current_program_num += 1;
    if (time_total < sum_program_length) {
      current_programInit = programGradientRainbowPoprocksInInit;
      current_program = programGradientRainbowPoprocksIn;
      enable_fade = false;
      break;
    }
    
    // 
    program_length = 60000;
    sum_program_length += program_length;
    current_program_num += 1;
    if (time_total < sum_program_length) {
      current_programInit = programGradientRainbowInit;
      current_program = programGradientRainbow;
      enable_fade = false;
      break;
    }
    

    // This program should always run for exactly one period. 
    // Current period is 10000
    program_length = 10000;
    sum_program_length += program_length;
    current_program_num += 1;
    if (time_total < sum_program_length) {
      current_programInit = programGradientRainbowPoprocksOutInit;
      current_program = programGradientRainbowPoprocksOut;
      enable_fade = false;
      break;
    }

    turnOffLeds();
    enterSleep();
    is_sleeping = true;
    break;
  }

  if (! is_sleeping) {
    
    if (current_program_num != last_program_num) {

      digitalWrite(LED_BUILTIN, HIGH);
      //Serial.print("Starting program #");
      //Serial.print(current_program_num);
      //Serial.print("\n");
      
      if (current_program_num == 1) {
        time_running -= RESET_DELAY;
      }
    
      endFadeOut();
      startFadeIn();
      current_programInit();
      last_program_num = current_program_num;
    }
    else {
      // Turn off the build in LED.
      digitalWrite(LED_BUILTIN, LOW);
      
      if (time_total >= (sum_program_length-FADE_LENGTH)) {
        startFadeOut();
      }
    }

    
    current_program();

    if (is_fade_out) {
      fadeOutLeds();
    }
    else if (is_fade_in) {
      fadeInLeds();
    }
    
    //Serial.println("Updating LEDs . . . ");
    updateLeds();
  }

  // update the cycle count.
  cycle += 1;

}




